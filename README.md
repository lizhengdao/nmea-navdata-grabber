# NMEA NavData Grabber
Simple Android-App to get NMEA-Data of a Android-Phones GPS-Receiver.
Data is published to other Network-Devices via UDP-Multicast.

# Example Usage:
1) Install and run the Android App.
2) Run the python-script in [udp_multicast_receiver](./udp_multicast_receiver):
```
python3 ./udp_multicast_receiver.py --port 4242 --group 224.51.105.104 --interface 192.168.2.104
```

and you should receive timestamped NMEA-Data like
```
...
('192.168.2.116', 4242)  b'2020-10-11T13:11:28.279Z,$GLGSV,3,2,09,69,36,317,,88,00,000,,85,16,015,,67,30,198,*6E\r'
('192.168.2.116', 4242)  b'2020-10-11T13:11:28.279Z,$GLGSV,3,3,09,68,57,253,*54\r'
('192.168.2.116', 4242)  b'2020-10-11T13:11:28.280Z,$GPGSA,A,1,,,,,,,,,,,,,,,*1E\r'
('192.168.2.116', 4242)  b'2020-10-11T13:11:28.280Z,$GPVTG,,T,,M,,N,,K,N*2C\r'
('192.168.2.116', 4242)  b'2020-10-11T13:11:28.280Z,$GPRMC,,V,,,,,,,,,,N*53\r'
...
```