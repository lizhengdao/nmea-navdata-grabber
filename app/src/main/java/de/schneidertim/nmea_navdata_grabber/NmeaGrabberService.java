package de.schneidertim.nmea_navdata_grabber;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.JobIntentService;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class NmeaGrabberService extends JobIntentService implements GpsStatus.NmeaListener {

    static final int NMEA_GRABBER_JOB_ID = 1000;
    static final boolean DEBUG = BuildConfig.DEBUG;

    private static final long GPS_UPDATE_FREQUENCY = 3000;

    private static MulticastSocket multicastSocket = null;
    private static InetAddress multicastGroup = null;

    public static final String MULTICASTGROUP = "multicastgroup";
    public static final String MULTICASTPORT = "multicastport";

    public static void joinMultiCastGroup(Context context, String multiCastGroup, int port) {
        Intent intent = new Intent(context, NmeaGrabberService.class);
        intent.putExtra(MULTICASTGROUP, multiCastGroup);
        intent.putExtra(MULTICASTPORT, port);
        enqueueWork(context, NmeaGrabberService.class, NMEA_GRABBER_JOB_ID, intent);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        LocationManager locationManager =
                (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                Log.d("", "location:" + location);
            }
        };

        if (       ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.e("", "Permission for ACCESS_FINE_LOCATION not granted!");
        }

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, GPS_UPDATE_FREQUENCY, 0, locationListener);

        locationManager.addNmeaListener(this);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if(DEBUG) Log.d("NmeaGrabberService", "onHandleWork");

        try {
            if(multicastSocket != null){
                if(DEBUG) Log.d("MultiCast", "Leaving "+multicastGroup.getHostAddress()+":"+multicastSocket.getLocalPort());
                multicastSocket.leaveGroup(multicastGroup);
                multicastSocket.close();
            }

            int port = intent.getIntExtra(MULTICASTPORT, 4242);
            multicastGroup = InetAddress.getByName(intent.getStringExtra(MULTICASTGROUP));

            if(DEBUG) Log.d("MultiCast", "Joining "+multicastGroup.getHostAddress() + ":" + port);
            multicastSocket = new MulticastSocket(port);
            multicastSocket.joinGroup(multicastGroup);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onNmeaReceived(long timestamp, String nmea) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = simpleDateFormat.format(new Date(timestamp));
        final String text = MessageFormat.format("{0},{1}", date, nmea);
        if(DEBUG) Log.d("NMEA", text);

        if(multicastSocket == null) return;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    //Your code goes here
                    byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
                    DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length, multicastGroup, multicastSocket.getLocalPort());
                    multicastSocket.send(datagramPacket);
                    Log.d("UDP", "send "+text);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}